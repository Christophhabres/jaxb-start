package at.spenger.jaxb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import at.spenger.jaxb.model.Book;
import at.spenger.jaxb.model.Bookstore;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application implements CommandLineRunner {

	private static final String BOOKSTORE_XML = "./bookstore-jaxb.xml";

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    private void schreiben() throws JAXBException {
    	ArrayList<Book> bookList = new ArrayList<Book>();

		// create books
		Book book1 = new Book();
		book1.setIsbn("978-0060554736");
		book1.setName("The Game");
		book1.setAuthor("Neil Strauss");
		book1.setPublisher("Harpercollins");
		bookList.add(book1);

		Book book2 = new Book();
		book2.setIsbn("978-3832180577");
		book2.setName("Feuchtgebiete");
		book2.setAuthor("Charlotte Roche");
		book2.setPublisher("Dumont Buchverlag");
		bookList.add(book2);

		// create bookstore, assigning book
		Bookstore bookstore = new Bookstore();
		bookstore.setName("Fraport Bookstore");
		bookstore.setLocation("Frankfurt Airport");
		bookstore.setBookList(bookList);

		// create JAXB context and instantiate marshaller
		JAXBContext context = JAXBContext.newInstance(Bookstore.class);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		// Write to System.out
		m.marshal(bookstore, System.out);

		// Write to File
		m.marshal(bookstore, new File(BOOKSTORE_XML));

    }
    
    
    private void lesen() throws JAXBException, FileNotFoundException {
    	System.out.println("Output from our XML File: ");
		// create JAXB context and instantiate marshaller
		JAXBContext context = JAXBContext.newInstance(Bookstore.class);
		Unmarshaller um = context.createUnmarshaller();
		Bookstore bookstore2 = (Bookstore) um.unmarshal(new FileReader(
				BOOKSTORE_XML));
		ArrayList<Book> list = bookstore2.getBooksList();
		list.forEach(b -> System.out.println("Book: " + b.getName() + " from "
					+ b.getAuthor()));
    }

	@Override
	public void run(String... arg0) throws Exception {
		schreiben();
		lesen();
	}
	
//	private void jsonschreiben() throws IOException {
//		JsonObject model = Json.createObjectBuilder()
//				   .add("firstName", "Duke")
//				   .add("lastName", "Java")
//				   .add("age", 18)
//				   .add("streetAddress", "100 Internet Dr")
//				   .add("city", "JavaTown")
//				   .add("state", "JA")
//				   .add("postalCode", "12345")
//				   .add("phoneNumbers", Json.createArrayBuilder()
//				      .add(Json.createObjectBuilder()
//				         .add("type", "mobile")
//				         .add("number", "111-111-1111"))
//				      .add(Json.createObjectBuilder()
//				         .add("type", "home")
//				         .add("number", "222-222-2222")))
//				   .build();
//		
//		FileWriter stWriter = new FileWriter("a.json");
//		
//		try (JsonWriter jsonWriter = Json.createWriter(stWriter)) {
//		   jsonWriter.writeObject(model);
//		}
//	}
	
}
